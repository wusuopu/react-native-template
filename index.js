import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';

// 选择显示 app 内容或者 storybook 内容
import App from './App';
AppRegistry.registerComponent(appName, () => App);


//import StorybookApp from './storybook'
//AppRegistry.registerComponent(appName, () => StorybookApp);
