项目使用 node v10 的版本进行开发。若需要安装多个版本的 node，建议使用 [nvm](https://github.com/nvm-sh/nvm)
然后在项目根目录下执行 `nvm use` 进行切换 node 的版本。

项目包含的工具： TypeScript, rematch(redux), recompose, styled-components, ant-design, react-navigation, storybook, jest, enzyme

## 安装依赖

```
yarn
cd ios
pod install
```


## 运行程序
运行开发服务器： `yarn start`

运行 ios app: 使用 Xcode 打开 ios/RNTSDemo.xcworkspace ，然后点击 Run

运行 android app: 使用 Android Studio 打开 android ，然后点击 Run

运行 src 目录下的所有测试用例： `yarn test src`

目录结构：

```
src/
  api/                    远程api相关
  components/             纯组件，没有连接redux
  constants/              定义常量
  containers/             与redux连接的组件
  containers/enhancers/   与hoc相关
  decorators/             函数装饰品
  lib/                    一些常用的函数
  mocks/                  一些生成mock数据的函数
  store/                  rematch(redux) store相关
  routes.tsx              定义应用的路由页面
  theme.tsx               定义应用的主题样式
  types.tsx               定义通用的 typescript 类型
```


