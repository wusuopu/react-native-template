import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import LandingScreen from './containers/landing-screen';
import HomeScreen from './containers/home-screen';

import Styles from './constants/styles'
import Sizes from './constants/sizes'

export const AppNavigator = createStackNavigator(
  {
    Landing: {
      screen: LandingScreen,
    },
    Home: {
      screen: HomeScreen,
      params: {
        statusBar: {},
      }
    },
  },
  {
    initialRouteName: 'Landing',
    // https://reactnavigation.org/docs/en/stack-navigator.html#navigationoptions-used-by-stacknavigator
    defaultNavigationOptions: {
      headerShown: false,       // 隐藏自带的导航栏
      gesturesEnabled: true,
      headerTitleAllowFontScaling: false,
      headerBackAllowFontScaling: false,
      headerBackTitle: null,
      // 配置头部导航栏的样式
      headerStyle: Styles.navBarHeaderStyle,
      headerTitleStyle: Styles.navBarHeaderTitleStyle,
      headerTitleContainerStyle: {
        justifyContent: 'center',
        left: 56,
        right: 56,
      },
      headerLeftContainerStyle: {
        transform: [{scale: Sizes.WINDOW_SCALE}]
      },
      headerRightContainerStyle: {
        transform: [{scale: Sizes.WINDOW_SCALE}]
      },
    }
  }
);
export default createAppContainer(AppNavigator);
