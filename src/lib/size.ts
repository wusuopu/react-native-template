import _ from 'lodash';
import Sizes from '../constants/sizes';

export default {
  convertSize(value: number, limit: {min?: number, max?: number} = {}): number {
    let newVal = value * Sizes.WINDOW_SCALE;
    if (!_.isUndefined(limit.min)) {
      if (newVal < limit.min) { newVal = limit.min }
    }
    if (!_.isUndefined(limit.max)) {
      if (newVal > limit.max) { newVal = limit.max }
    }
    return newVal;
  },
  convertLandScapeSize(value: number, limit: {min?: number, max?: number} = {}): number {
    let newVal = value * Sizes.LANDSCAPE_WINDOW_SCALE;
    if (!_.isUndefined(limit.min)) {
      if (newVal < limit.min) { newVal = limit.min }
    }
    if (!_.isUndefined(limit.max)) {
      if (newVal > limit.max) { newVal = limit.max }
    }
    return newVal;
  },
};
