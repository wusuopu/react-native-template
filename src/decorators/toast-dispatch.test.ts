import Portal from '@ant-design/react-native/lib/portal';
import ToastDispatchDecorator from './toast-dispatch'

describe('ToastDispatchDecorator', () => {
  describe('error decorator', () => {
    it('没有异常不显示 toast', async () => {
      let dispatch = {app: {ui$toast: jest.fn(__ => 1)}}
      let func = ToastDispatchDecorator.error(dispatch, async () => {
        return true
      })
      await func()

      expect(dispatch.app.ui$toast.mock.calls.length).toEqual(0)
    })
    it('出现异常显示 toast', async () => {
      let dispatch = {app: {ui$toast: jest.fn(__ => 1)}}
      let func = ToastDispatchDecorator.error(dispatch, async () => {
        throw new Error('error')
      })
      await func()

      expect(dispatch.app.ui$toast.mock.calls.length).toEqual(1)
    })
  })

  describe('loading decorator', () => {
    it('无异常的情况', async () => {
      let dispatch = {app: {ui$toast: jest.fn(__ => 1)}}
      let portalRemove = jest.spyOn(Portal, 'remove')

      let func = ToastDispatchDecorator.loading(dispatch, async () => {
        return true
      })
      await func()

      expect(dispatch.app.ui$toast.mock.calls.length).toEqual(1)
      expect(portalRemove).toHaveBeenCalledTimes(1)
      portalRemove.mockRestore()
    })
    it('有异常的情况', async () => {
      let dispatch = {app: {ui$toast: jest.fn(__ => 1)}}
      let portalRemove = jest.spyOn(Portal, 'remove')
      let func = ToastDispatchDecorator.loading(dispatch, async () => {
        throw new Error('error')
      })
      await func()

      expect(dispatch.app.ui$toast.mock.calls.length).toEqual(1)
      expect(portalRemove).toHaveBeenCalledTimes(1)
      portalRemove.mockRestore()
    })
  })

  describe('errorAndLoading decorator', () => {
    it('无异常的情况，只显示loading', async () => {
      let dispatch = {app: {ui$toast: jest.fn(__ => 1)}}
      let portalRemove = jest.spyOn(Portal, 'remove')

      let func = ToastDispatchDecorator.errorAndLoading(dispatch, async () => {
        return true
      })
      await func()

      expect(dispatch.app.ui$toast.mock.calls.length).toEqual(1)
      expect(dispatch.app.ui$toast.mock.calls[0][0].action).toEqual('loading')
      expect(portalRemove).toHaveBeenCalledTimes(1)
      portalRemove.mockRestore()
    })
    it('有异常的情况，先显示loading，再显示error', async () => {
      let dispatch = {app: {ui$toast: jest.fn(__ => 1)}}
      let portalRemove = jest.spyOn(Portal, 'remove')
      let func = ToastDispatchDecorator.errorAndLoading(dispatch, async () => {
        throw new Error('error')
      })
      await func()

      expect(dispatch.app.ui$toast.mock.calls.length).toEqual(2)
      expect(dispatch.app.ui$toast.mock.calls[0][0].action).toEqual('loading')
      expect(dispatch.app.ui$toast.mock.calls[1][0].action).toEqual('fail')
      expect(portalRemove).toHaveBeenCalledTimes(1)
      portalRemove.mockRestore()
    })
  })
})
