import Portal from '@ant-design/react-native/lib/portal';
import _ from 'lodash';
import { Dispatch } from '../store';


export default {
  // 调用 api 过程中出错则显示 toast error
  error(dispatch: Dispatch, func: Function) {
    return async (...args) => {
      try {
        return await func(...args)
      } catch (err) {
        console.debug(err)
        dispatch.app.ui$toast({content: err.message, action: 'fail'})
      }
    }
  },
  // 调用 api 过程中显示全局的 loading toast
  loading(dispatch: Dispatch, func: Function, toastParams?: {content: any}) {
    return async (...args) => {
      let key = await dispatch.app.ui$toast(_.assign({content: '', action: 'loading', duration: 0}, toastParams))
      try {
        return await func(...args)
      } catch (error) {
      } finally {
        Portal.remove(key)
      }
    }
  },
  errorAndLoading(dispatch: Dispatch, func: Function) {
    return this.loading(dispatch, this.error(dispatch, func))
  }
}
