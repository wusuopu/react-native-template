import { NavigationScreenProp } from 'react-navigation';

interface NavigationProp<S> extends NavigationScreenProp<S> {
  push: (name: string, params?: object) => void;
  replace: (name: string, params?: object) => void;
  pop: (n?: number) => void;
}

export type ScreenProps = {
  navigation: NavigationProp<any>;
};
