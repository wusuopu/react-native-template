import { Dimensions, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';

let WINDOW_WIDTH = Dimensions.get('window').width;
let WINDOW_HEIGHT = Dimensions.get('window').height;
if (WINDOW_WIDTH > WINDOW_HEIGHT) {
  // App启动时手机是横屏状态，则交互两个变量的值
  WINDOW_WIDTH = WINDOW_WIDTH ^ WINDOW_HEIGHT;
  WINDOW_HEIGHT = WINDOW_WIDTH ^ WINDOW_HEIGHT;
  WINDOW_WIDTH = WINDOW_WIDTH ^ WINDOW_HEIGHT;
}
// 横屏的尺寸
let LANDSCAPE_WINDOW_WIDTH = WINDOW_HEIGHT;
let LANDSCAPE_WINDOW_HEIGHT = WINDOW_WIDTH;

let SOFT_MENU_BAR_HEIGHT = 0;   // android soft menu 高度
let SMART_BAR_HEIGHT = 0;       // 魅族 smart bar 的高度

let STATUS_BAR_HEIGHT = 20;          // 状态栏高度
let NAV_BAR_HEIGHT = 44;             // 头部导航栏高度
let TAB_BAR_HEIGHT = 49;             // 底部标签栏高度
if (Platform.OS === 'ios') {
  let isIPhoneX = !Platform.isPad && !Platform.isTVOS &&
                  (WINDOW_WIDTH === 375 && WINDOW_HEIGHT === 812) ||
                  (WINDOW_WIDTH === 414 && WINDOW_HEIGHT === 896);
  if (isIPhoneX) {
    // iPhoneX 有刘海， StatusBar 的高度不同
    STATUS_BAR_HEIGHT = 44;
  }
}

// 设计稿通常是以 iPhone 6 尺寸为参考
const BASE_WIDTH = 375;       // the screen width of iPhone 6
const BASE_HEIGHT = 667;      // the screen height of iPhone 6

let WINDOW_SCALE = WINDOW_HEIGHT / BASE_HEIGHT;        // 相对于 iPhone 6 的屏幕比例
let LANDSCAPE_WINDOW_SCALE = WINDOW_WIDTH / BASE_WIDTH;

if (Platform.OS === 'android') {
  const ExtraDimensions = require('react-native-extra-dimensions-android');
  SOFT_MENU_BAR_HEIGHT = ExtraDimensions.get('SOFT_MENU_BAR_HEIGHT');
  SMART_BAR_HEIGHT = ExtraDimensions.get('SMART_BAR_HEIGHT');   // android 的状态栏高度通常是25
  WINDOW_WIDTH = ExtraDimensions.get('REAL_WINDOW_WIDTH');
  WINDOW_HEIGHT = ExtraDimensions.get('REAL_WINDOW_HEIGHT');
  if (WINDOW_WIDTH > WINDOW_HEIGHT) {
    // App启动时手机是横屏状态，则交互两个变量的值
    WINDOW_WIDTH = WINDOW_WIDTH ^ WINDOW_HEIGHT;
    WINDOW_HEIGHT = WINDOW_WIDTH ^ WINDOW_HEIGHT;
    WINDOW_WIDTH = WINDOW_WIDTH ^ WINDOW_HEIGHT;
  }
  WINDOW_HEIGHT = WINDOW_HEIGHT - SOFT_MENU_BAR_HEIGHT;
  STATUS_BAR_HEIGHT = ExtraDimensions.get('STATUS_BAR_HEIGHT');
  if (SMART_BAR_HEIGHT) {
    WINDOW_HEIGHT -= SMART_BAR_HEIGHT;
  }
  // Android横屏后，虚拟按键的位置也发生了变化
  // 判断设备是否为pad，只有pad虚拟按键才会旋转
  if (DeviceInfo.isTablet()) {
    LANDSCAPE_WINDOW_WIDTH = WINDOW_HEIGHT + SOFT_MENU_BAR_HEIGHT;
    LANDSCAPE_WINDOW_HEIGHT = WINDOW_WIDTH - SOFT_MENU_BAR_HEIGHT;
  } else {
    LANDSCAPE_WINDOW_WIDTH = WINDOW_HEIGHT;
    LANDSCAPE_WINDOW_HEIGHT = WINDOW_WIDTH;
  }
  // 在魅族手机中 Smart Bar 会随着屏幕进行旋转
  if (SMART_BAR_HEIGHT) {
    LANDSCAPE_WINDOW_WIDTH += SMART_BAR_HEIGHT;
    LANDSCAPE_WINDOW_HEIGHT -= SMART_BAR_HEIGHT;
  }

  WINDOW_SCALE = WINDOW_HEIGHT / BASE_HEIGHT;
  LANDSCAPE_WINDOW_SCALE = LANDSCAPE_WINDOW_HEIGHT / BASE_WIDTH;
}

const NAV_HEADER_HEIGHT = NAV_BAR_HEIGHT + STATUS_BAR_HEIGHT;

const SIDE_MENU_WIDTH = WINDOW_WIDTH / 4 * 3;   // 侧边弹出菜单宽度

export default {
  WINDOW_WIDTH,
  WINDOW_HEIGHT,
  LANDSCAPE_WINDOW_WIDTH,
  LANDSCAPE_WINDOW_HEIGHT,
  WINDOW_SCALE,
  LANDSCAPE_WINDOW_SCALE,

  SIDE_MENU_WIDTH,

  STATUS_BAR_HEIGHT,
  NAV_BAR_HEIGHT,
  TAB_BAR_HEIGHT,
  NAV_HEADER_HEIGHT,
  SOFT_MENU_BAR_HEIGHT,
  SMART_BAR_HEIGHT,
};
