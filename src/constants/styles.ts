import Sizes from './sizes';
import size from '../lib/size';

const navBarHeaderStyle = {
  height: size.convertSize(Sizes.NAV_BAR_HEIGHT, {min: Sizes.NAV_BAR_HEIGHT}),
};
const navBarHeaderWithoutBorderStyle = {
  borderBottomWidth: 0,
  height: size.convertSize(Sizes.NAV_BAR_HEIGHT, {min: Sizes.NAV_BAR_HEIGHT}),
}
const navBarHeaderTitleStyle = {
  fontSize: size.convertSize(18),
  color: 'rgba(0, 0, 0, .9)',
  marginHorizontal: 16,
};

export default {
  navBarHeaderStyle,
  navBarHeaderTitleStyle,
  navBarHeaderWithoutBorderStyle,
}
