import { NavigationActions, StackActions } from 'react-navigation';
import _ from 'lodash';

// https://reactnavigation.org/docs/en/navigating-without-navigation-prop.html
let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName: string | object, params?: object, action?: object): boolean {
  if (_.isString(routeName)) {
    return _navigator.dispatch(
      NavigationActions.navigate({
        routeName,
        params,
        action,
      })
    );
  } else {
    return _navigator.dispatch(routeName)
  }
}

function reset(routeName: string, params?: object): boolean {
  return _navigator.dispatch(StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({routeName, params})]
  }))
}
function push(routeName: string, params?: object): boolean {
  return _navigator.dispatch(StackActions.push({
    routeName,
    params,
  }))
}
function back(key?: string): boolean {
  return _navigator.dispatch(NavigationActions.back({key}))
}

// add other navigation functions that you need and export them

export default {
  navigate,
  reset,
  push,
  back,
  setTopLevelNavigator,
};
