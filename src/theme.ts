import { Theme as ATheme } from '@ant-design/react-native/lib/style';
import _ from 'lodash';
import { Theme } from 'styled-system';
import size from './lib/size';

const brandPrimary = '#108ee9';
const brandPrimaryTap = '#1284d6';
export const AntdTheme: ATheme = {
    // 文字色
    color_text_base: '#000000',
    color_text_base_inverse: '#ffffff',
    color_text_secondary: '#a4a9b0',
    color_text_placeholder: '#bbbbbb',
    color_text_disabled: '#bbbbbb',
    color_text_caption: '#888888',
    color_text_paragraph: '#333333',
    color_link: brandPrimary,
    // 背景色
    fill_base: '#ffffff',
    fill_body: '#f5f5f9',
    fill_tap: '#dddddd',
    fill_disabled: '#dddddd',
    fill_mask: 'rgba(0, 0, 0, .4)',
    color_icon_base: '#cccccc',
    fill_grey: '#f7f7f7',
    // 透明度
    opacity_disabled: '0.3',
    // 全局/品牌色
    brand_primary: brandPrimary,
    brand_primary_tap: brandPrimaryTap,
    brand_success: '#6abf47',
    brand_warning: '#f4333c',
    brand_error: '#f4333c',
    brand_important: '#ff5b05',
    brand_wait: '#108ee9',
    // 边框色
    border_color_base: '#dddddd',
    // 字体尺寸
    // ---
    font_size_icontext: size.convertSize(10),
    font_size_caption_sm: size.convertSize(12),
    font_size_base: size.convertSize(14),
    font_size_subhead: size.convertSize(15),
    font_size_caption: size.convertSize(16),
    font_size_heading: size.convertSize(17),
    // 圆角
    // ---
    radius_xs: size.convertSize(2),
    radius_sm: size.convertSize(3),
    radius_md: size.convertSize(5),
    radius_lg: size.convertSize(7),
    // 边框尺寸
    // ---
    border_width_sm: size.convertSize(0.5),
    border_width_md: size.convertSize(1),
    border_width_lg: size.convertSize(2),
    // 间距
    // ---
    // 水平间距
    h_spacing_sm: size.convertSize(5),
    h_spacing_md: size.convertSize(8),
    h_spacing_lg: size.convertSize(15),
    // 垂直间距
    v_spacing_xs: size.convertSize(3),
    v_spacing_sm: size.convertSize(6),
    v_spacing_md: size.convertSize(9),
    v_spacing_lg: size.convertSize(15),
    v_spacing_xl: size.convertSize(21),
    // 高度
    // ---
    line_height_base: size.convertSize(1),
    line_height_paragraph: size.convertSize(1.5),
    // 图标尺寸
    // ---
    icon_size_xxs: size.convertSize(15),
    icon_size_xs: size.convertSize(18),
    icon_size_sm: size.convertSize(21),
    icon_size_md: size.convertSize(22),
    icon_size_lg: size.convertSize(36),
    // 动画缓动
    // ---
    ease_in_out_quint: 'cubic_bezier(0.86, 0, 0.07, 1)',
    // 组件变量
    // ---
    actionsheet_item_height: size.convertSize(50),
    actionsheet_item_font_size: size.convertSize(18),
    // button
    button_height: size.convertSize(42),
    button_font_size: size.convertSize(18),
    button_height_sm: size.convertSize(23),
    button_font_size_sm: size.convertSize(12),
    primary_button_fill: brandPrimary,
    primary_button_fill_tap: '#0e80d2',
    ghost_button_color: brandPrimary,
    ghost_button_fill_tap: brandPrimary + '99',
    warning_button_fill: '#e94f4f',
    warning_button_fill_tap: '#d24747',
    link_button_fill_tap: '#dddddd',
    link_button_font_size: size.convertSize(16),
    // modal
    modal_font_size_heading: size.convertSize(18),
    modal_button_font_size: size.convertSize(18),
    modal_button_height: size.convertSize(50),
    // list
    list_title_height: size.convertSize(30),
    list_item_height_sm: size.convertSize(35),
    list_item_height: size.convertSize(44),
    // input
    input_label_width: size.convertSize(17),
    input_font_size: size.convertSize(17),
    input_color_icon: '#cccccc',
    input_color_icon_tap: brandPrimary,
    // tabs
    tabs_color: brandPrimary,
    tabs_height: size.convertSize(42),
    tabs_font_size_heading: size.convertSize(15),
    // segmented_control
    segmented_control_color: brandPrimary,
    segmented_control_height: size.convertSize(27),
    segmented_control_fill_tap: brandPrimary + '10',
    // tab_bar
    tab_bar_fill: '#ebeeef',
    tab_bar_height: size.convertSize(50),
    // toast
    toast_fill: 'rgba(0, 0, 0, .8)',
    // search_bar
    search_bar_fill: '#efeff4',
    search_bar_height: size.convertSize(44),
    search_bar_input_height: size.convertSize(28),
    search_bar_font_size: size.convertSize(15),
    search_color_icon: '#bbbbbb',
    // notice_bar
    notice_bar_fill: '#fffada',
    notice_bar_height: size.convertSize(36),
    // switch
    switch_fill: '#4dd865',
    // tag
    tag_height: size.convertSize(25),
    tag_small_height: size.convertSize(15),
    // picker
    option_height: size.convertSize(42),
    toast_zindex: 1999,
    action_sheet_zindex: 1000,
    popup_zindex: 999,
    modal_zindex: 999
};

const StyledTheme: Theme = {
  // https://styled-system.com/guides/theming
  borders: [],
  borderWidths: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((v) => size.convertSize(v)),
  colors: {
    primary: '#2A82E4',
    text: '#333333',
  },
  fontSizes: [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20].map((v) => size.convertSize(v)),
  radii: [0, 2, 3, 5, 7].map((v) => size.convertSize(v)),
  shadows: ['none', '1px 1px 5px rgba(0, 0, 0, 0.09)'],
  sizes: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((v) => size.convertSize(v)),
  space: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((v) => size.convertSize(v)),
  elevations: [0, 1, 2, 3, 4, 5, 6, 7, 8 ,9, 10],
};
StyledTheme.radii.xs = StyledTheme.radii[1];
StyledTheme.radii.sm = StyledTheme.radii[2];
StyledTheme.radii.md = StyledTheme.radii[3];
StyledTheme.radii.lg = StyledTheme.radii[4];

export default StyledTheme;
