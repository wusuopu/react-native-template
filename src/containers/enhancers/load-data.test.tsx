import React from 'react';
import { shallow } from 'enzyme';
import { Text } from 'react-native';
import LoadDataEnhancer from './load-data';


describe('LoadDataEnhancer', () => {
  it('显示 loading 动画', async () => {
    let promise
    let Comp = LoadDataEnhancer(Text)
    let render = shallow(<Comp onFetchData={() => {
      promise = Promise.resolve({})
      return promise
    }} />);
    // 未加载数据则显示 loading
    expect(render.state().loaded).toBeFalsy()
    console.log(render.text(), render.state())
    await Promise.all(promise)
    render.update()
    // 加载数据完成则显示对应组件
    expect(render.text()).toEqual('<Text />')
    expect(render.state().loaded).toBeTruthy()
    expect(render.state().data).toEqual({})
  })
})

