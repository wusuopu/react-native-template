import { StatusBarProps } from 'react-native';
import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';
import { Dispatch } from '../../store';

const mapDispatch = (dispatch: Dispatch) => ({
  statusBar$set(status: StatusBarProps) {
    dispatch.app.ui$statusBar(status)
  }
})

export type StatusBarEnhancerProps = ReturnType<typeof mapDispatch>
export default compose(
  connect(null, mapDispatch),
  lifecycle({
    componentDidMount() {
      // 进入当前页面之后更新状态栏
      let statusBar = this.props.navigation.getParam('statusBar')
      statusBar && this.props.statusBar$set(statusBar)
    }
  })
)
