import React from 'react';
import LoadingIndicator from '../loading-indicator';

type Props = {
  onFetchData: () => Promise<any>;
}
type State = {
  loaded: boolean;
  data?: any;
}

// 首次进入页面显示加载动画
export default function LoadDataEnhancer (Comp: React.ComponentType<{data: any}>) {
  class LoadDataComponent extends React.Component<Props, State> {
    state = {
      loaded: false,
      data: undefined,
    }

    async componentDidMount() {
      let data = await this.props.onFetchData()
      if (data) {
        this.setState({data, loaded: true})
      }
    }

    render() {
      if (!this.state.loaded) {
        return (
          <LoadingIndicator />
        )
      }

      return (
        <Comp {...this.props} data={this.state.data} />
      )
    }
  }

  return LoadDataComponent
}
