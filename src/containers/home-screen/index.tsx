import React from 'react';
import Layout from '../layout';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { ScreenProps } from '../../types';
import StatusBarEnhancer from '../enhancers/status-bar';
import TabBar from '../../components/tab-bar';
import View from '../../components/base/view';

class HomeScreen extends React.PureComponent<ScreenProps> {
  render() {
    return (
      <Layout headerShown={false}>
        <View flex={1} />
        <TabBar
          items={[
            {title: '抢单', icon: 'edit',},
            {title: '我的订单', icon: 'form', selected: true },
            {title: '个人', icon: 'copy', },
          ]}
        />
      </Layout>
    )
  }
}

export default compose(
  StatusBarEnhancer,
  connect(null, null)
)(HomeScreen);
