import React from 'react';
import ActivityIndicator from '@ant-design/react-native/lib/activity-indicator';
import View from '../components/base/view';

export default (props) => (
  <View py={10} flex={1} alignItems="center" justifyContent="center" {...props}>
    <ActivityIndicator size="large" text="数据加载中..." />
  </View>
)
