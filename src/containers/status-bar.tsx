import React from 'react';
import { Platform, StatusBar as RNStatusBar, StatusBarProps } from 'react-native';
import { connect } from 'react-redux';
import { iRootState } from '../store';
import View from '../components/base/view';
import Sizes from '../constants/sizes';


class StatusBar extends React.PureComponent<StatusBarProps> {
  render() {
    let ele
    if (this.props.backgroundColor && !this.props.hidden && Platform.OS === 'ios') {
      ele = (
        <View
          bg={this.props.backgroundColor}
          height={Sizes.STATUS_BAR_HEIGHT}
          width="100%"
          position="absolute"
          left={0}
          top={0}
          zIndex={10000}
        />
      )
    }
    return (
      <>
        {ele}
        <RNStatusBar {...this.props} />
      </>
    )
  }
}
export default connect(
  (state: iRootState) => state.app.statusBar,
  null,
)(StatusBar);
