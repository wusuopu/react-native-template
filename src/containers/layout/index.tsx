import React from 'react';
import { withNavigation } from 'react-navigation';
import Navbar, { NavProps } from '../../components/navbar';
import SafeView from '../../components/base/safe-view';
import View from '../../components/base/view';
import { ScreenProps } from '../../types'

type LayoutProps = ScreenProps & {
  headerShown?: boolean,
  navOptions?: NavProps,
}

class Layout extends React.PureComponent<LayoutProps> {
  static defaultProps = {
    headerShown: true,
    navOptions: {},
  }

  handleLeftPress = () => {
    this.props.navigation.goBack()
  }
  renderNavbar() {
    let root = this.props.navigation.dangerouslyGetParent()
    return (
      <Navbar
        onLeftPress={this.handleLeftPress}
        backButtonShown={root?.state?.index > 0}    // 当前不是第一个页面，则显示返回按钮
        {...this.props.navOptions}>
      </Navbar>
    )
  }
  render() {
    return (
      <SafeView justifyContent="flex-start">
        {this.props.headerShown && this.renderNavbar()}
        <View flex={1} flexDirection="column">
          {this.props.children}
        </View>
      </SafeView>
    )
  }
}


export default withNavigation(Layout);
