import React from 'react';
import { NavigationActions } from 'react-navigation';
import SafeView from '../components/base/safe-view';
import Text from '../components/base/text';
import { ScreenProps } from '../types';

type Props = ScreenProps;

export default class LandingScreen extends React.Component<Props> {
  async componentDidMount() {
    // 1. 先显示 splash 页面，等待数据加载
    this.props.navigation.reset([NavigationActions.navigate({routeName: 'Home'})])
  }
  render() {
    return (
      <SafeView>
        <Text>加载中...</Text>
      </SafeView>
    )
  }
}
