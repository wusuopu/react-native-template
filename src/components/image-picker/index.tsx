import React from 'react';
import CameraRoll from "@react-native-community/cameraroll";
import { Dimensions, Modal, ScrollView } from 'react-native';
import { Icon } from '@ant-design/react-native';
import _ from 'lodash';
import Navbar from '../navbar';
import View from '../base/view';
import SafeView from '../base/safe-view';
import TouchItem from '../base/touch-item';
import Image from '../base/image';
import size from '../../lib/size';

type FileType = {
  uri: string;
  filename: string;
  height?: number;
  width?: number;
  type: 'image'|'video';
}
type FileFormType = {   // 文件上传表单的数据类型
  uri: string;
  name: string;
  type: string;
}

type Props = {
  size: number;
  cameraPickerProps: {
    first: number;
    after?: string;
    groupTypes: "Album"|"All"|"Event"|"Faces"|"Library"|"PhotoStream"|"SavedPhotos";
    groupName?: string;
    assetType: 'All'|'Photos'|'Videos';
    mimeTypes: string[];
    fromTime: number;
    toTime: number;
  },
  onImageClick?: (index: number, file: FileType) => Promise<any>;
  onCancel?: () => void;
  onChange?: (file: FileFormType) => Promise<any>;
}
type State = {
  modalVisible: boolean;
  photos: any[];
  selectedIndex: number;
  selectedImage?: FileType;
}

export default class ImagePicker extends React.PureComponent<Props, State> {
  imageMargin: number;
  imageSize: number;

  static defaultProps = {
    size: size.convertSize(85),
    cameraPickerProps: {
      first: 50,
      assetType: 'Photos',
      mimeTypes: ['image/jpeg', 'image/png'],
    },
  }

  state = {
    modalVisible: false,
    photos: [],
    selectedIndex: -1,
    selectedImage: undefined,
  }

  constructor(props) {
    super(props)

    let { width } = Dimensions.get('window');
    this.imageMargin = size.convertSize(5)
    this.imageSize = (width - 8 * this.imageMargin) / 4
  }

  showModal = () => {
    this.setState({modalVisible: true})
  }
  hideModal = () => {
    this.setState({modalVisible: false, selectedIndex: -1})
  }

  handleClick = async () => {
    this.showModal()
    this.setState({photos: []})
    let photos = _.map(
      (await CameraRoll.getPhotos(this.props.cameraPickerProps)).edges,
      (item) => {
        return _.assign({type: item.node.type}, item.node.image)
    })
    this.setState({photos})
  }
  handleOk = () => {
    if (this.state.selectedIndex < 0) {
      return
    }
    let photo: FileType = this.state.photos[this.state.selectedIndex]
    this.hideModal()
    this.setState({selectedImage: photo})

    let type = (/\.png$/i).test(photo.filename) ? 'png' : 'jpeg'
    let file = {
      uri: photo.uri,
      name: photo.filename,
      type: `image/${type}`,
    }
    this.props.onChange && this.props.onChange(file)
  }
  handleCancel = () => {
    this.hideModal()
    this.props.onCancel && this.props.onCancel()
  }

  handleSelect = (index: number) => {
    this.setState({selectedIndex: index})
    this.props.onImageClick && this.props.onImageClick(index, this.state.photos[index])
  }
  renderPhoto = (photo, index) => {
    let checked = this.state.selectedIndex === index
    return (
      <TouchItem
        key={index}
        size={this.imageSize}
        justifyContent="center"
        alignItems="center"
        backgroundColor="#cccccc"
        m={5}
        position="relative"
        onPress={() => this.handleSelect(index)}
      >
        <Image
          width={this.imageSize}
          height={this.imageSize}
          resizeMode="contain"
          source={{uri: photo.uri}}
        />
        {checked ? <View position="absolute" right={0} top={0}><Icon name="check-circle" color="green" /></View> : null}
      </TouchItem>
    )
  }
  renderModal() {
    return (
      <SafeView backgroundColor="#ffffff" flex={1}>
        <Navbar
          left="选择"
          onLeftPress={this.handleOk}
          right="取消"
          onRightPress={this.handleCancel}
        />
        <ScrollView>
          <View flexDirection="row" flexWrap="wrap">
            {
              _.map(this.state.photos, this.renderPhoto)
            }
          </View>
        </ScrollView>
      </SafeView>
    )
  }

  render() {
    let iconSize = this.props.size - 20
    if (iconSize <= 0) { iconSize = this.props.size }

    let icon
    if (this.state.selectedImage) {
      icon = (
        <Image
          resizeMode="stretch"
          size={this.props.size - 4}
          source={{uri: this.state.selectedImage.uri}}
        />
      )
    } else {
      icon = (
        <Icon size={iconSize} name="plus" />
      )
    }

    return (
      <React.Fragment>
        <TouchItem
          size={this.props.size}
          justifyContent="center"
          alignItems="center"
          borderWidth={1}
          borderRadius={3}
          borderColor="#cccccc"
          onPress={this.handleClick}
          mt={1}
        >
          {icon}
        </TouchItem>
        <Modal
          transparent={false}
          animationType="slide"
          visible={this.state.modalVisible}
        >
          {this.renderModal()}
        </Modal>
      </React.Fragment>
    )
  }
}
