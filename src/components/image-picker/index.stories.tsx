import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { action } from '../../../storybook/addons/actions';
import { ScrollView } from 'react-native';
import SafeView from '../base/safe-view';
import ImagePicker from '.';

storiesOf('ImagePicker', module)
.add('default', () => (
  <SafeView>
    <ScrollView>
      <ImagePicker
        onImageClick={action('click')}
        onCancel={action('cancel')}
        onChange={action('change')}
      />
    </ScrollView>
  </SafeView>
))
