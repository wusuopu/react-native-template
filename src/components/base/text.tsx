import React from 'react';
import { Text as RNText } from 'react-native';
import styled from 'styled-components/native';
import _ from 'lodash';
import { color, flexbox, space, layout, position, background, border, shadow, typography } from 'styled-system';
import { ColorProps, FlexboxProps, SpaceProps, LayoutProps, PositionProps, BackgroundProps, BordersProps, ShadowProps, TypographyProps } from 'styled-system';

export type TextProps = React.ComponentProps<typeof RNText> & ColorProps & FlexboxProps & SpaceProps & LayoutProps & PositionProps & BackgroundProps & BordersProps & ShadowProps & TypographyProps;
RNText.defaultProps = _.assign({}, RNText.defaultProps, {allowFontScaling: false});     // 禁止字段根据系统的配置进行缩放

const Text = styled.Text<TextProps>`
  ${color};
  ${flexbox};
  ${space};
  ${layout};
  ${position};
  ${background};
  ${border};
  ${shadow};
  ${typography};
`;
Text.defaultProps = {
  allowFontScaling: false,
}
export default Text;
