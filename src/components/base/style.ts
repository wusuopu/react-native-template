import { style } from 'styled-system';

// android 平台的 shadow 效果
export const elevation = style({
  prop: 'androidElevation',
  cssProperty: 'elevation',
  key: 'elevations',
})
export type ElevationProps = {
  androidElevation?: number;
}
