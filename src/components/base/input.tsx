import React from 'react';
import { TextInput } from 'react-native';
import InputItem, { InputItemProps } from '@ant-design/react-native/lib/input-item';
import styled from 'styled-components/native';
import { color, space, layout, position, background, border, shadow, typography } from 'styled-system';
import { ColorProps, SpaceProps, LayoutProps, PositionProps, BackgroundProps, BordersProps, ShadowProps, TypographyProps } from 'styled-system';
import size from '../../lib/size';


// antd 的输入框
export type AntdInputProps = InputItemProps & ColorProps & SpaceProps & LayoutProps & PositionProps & BackgroundProps & BordersProps & ShadowProps & TypographyProps;
export const AntdInput = styled(InputItem)<AntdInputProps>`
  ${color};
  ${space};
  ${layout};
  ${position};
  ${background};
  ${border};
  ${shadow};
  ${typography};
`;
AntdInput.defaultProps = {
  allowFontScaling: false,
  borderWidth: 0.5,
  borderRadius: 3,
  paddingY: size.convertSize(3),
  paddingX: size.convertSize(5),
  height: size.convertSize(42),
  fontSize: size.convertSize(16),
}


type MyInputProps = React.ComponentProps<typeof TextInput> & ColorProps & SpaceProps & LayoutProps & PositionProps & BackgroundProps & BordersProps & ShadowProps & TypographyProps;
const MyInput = styled.TextInput<MyInputProps>`
  ${color};
  ${space};
  ${layout};
  ${position};
  ${background};
  ${border};
  ${shadow};
  ${typography};
`;
MyInput.defaultProps = {
  allowFontScaling: false,
  borderWidth: 0.5,
  borderRadius: 3,
  paddingY: size.convertSize(3),
  paddingX: size.convertSize(5),
  height: size.convertSize(42),
  fontSize: size.convertSize(16),
}

export type InputProps = MyInputProps & {
  type?: 'password' | 'number' | 'phone';
  onVirtualKeyboardConfirm?: Function;
};
export default class Input extends React.PureComponent<InputProps> {
  onKeyPress = (ev) => {
    if (ev.nativeEvent.key === 'Enter') {
      this.props.onVirtualKeyboardConfirm && this.props.onVirtualKeyboardConfirm()
    } else {
      this.props.onKeyPress && this.props.onKeyPress(ev)
    }
  }
  render() {
    const { type, ...props } = this.props;
    if (type === 'password') {
      props.secureTextEntry = true
    } else if (type === 'number') {
      props.keyboardType = 'numeric'
    } else if (type === 'phone') {
      props.keyboardType = 'phone-pad'
      props.maxLength = 11
    }
    props.onKeyPress = this.onKeyPress
    return (
      <MyInput {...props} />
    )
  }
};
