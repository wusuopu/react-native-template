import styled from 'styled-components/native';
import { color, flexbox, space, layout, position, background, border, shadow } from 'styled-system';
import { ColorProps, FlexboxProps, SpaceProps, LayoutProps, PositionProps, BackgroundProps, BordersProps, ShadowProps } from 'styled-system';

export type SafeViewProps = ColorProps & FlexboxProps & SpaceProps & LayoutProps & PositionProps & BackgroundProps & BordersProps & ShadowProps;
const SafeView = styled.SafeAreaView<SafeViewProps>`
  ${space};
  ${color};
  ${flexbox};
  ${layout};
  ${position};
  ${background};
  ${border};
  ${shadow};
`;
SafeView.defaultProps = {
  flex: 1,
  flexDirection: "column",
}

export default SafeView;
