import TextareaItem, { TextareaItemProps } from '@ant-design/react-native/lib/textarea-item'
import styled from 'styled-components/native';
import { color, space, layout, position, background, border, shadow, typography } from 'styled-system';
import { ColorProps, SpaceProps, LayoutProps, PositionProps, BackgroundProps, BordersProps, ShadowProps, TypographyProps } from 'styled-system';


export type TextAreaProps = TextareaItemProps & ColorProps & SpaceProps & LayoutProps & PositionProps & BackgroundProps & BordersProps & ShadowProps & TypographyProps;

const TextArea = styled(TextareaItem)<TextAreaProps>`
  ${color};
  ${space};
  ${layout};
  ${position};
  ${background};
  ${border};
  ${shadow};
  ${typography};
`;
TextArea.defaultProps = {
  allowFontScaling: false,
}
export default TextArea;
