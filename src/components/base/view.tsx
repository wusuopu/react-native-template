import styled from 'styled-components/native';
import { color, flexbox, space, layout, position, background, border, shadow } from 'styled-system';
import { ColorProps, FlexboxProps, SpaceProps, LayoutProps, PositionProps, BackgroundProps, BordersProps, ShadowProps } from 'styled-system';
import {elevation, ElevationProps} from './style'


export type ViewProps = ColorProps & FlexboxProps & SpaceProps & LayoutProps & PositionProps & BackgroundProps & BordersProps & ShadowProps & ElevationProps;
const View = styled.View<ViewProps>`
  ${space};
  ${color};
  ${flexbox};
  ${layout};
  ${position};
  ${background};
  ${border};
  ${shadow};
  ${elevation};
`;

export const Row = styled(View).attrs({
  flexDirection: 'row',
})<ViewProps>``;
export const Col = styled(View).attrs({
  flexDirection: 'column',
})<ViewProps>``;

export default View;
