import styled from 'styled-components/native';
import { color, flexbox, space, layout, position, background, border, shadow } from 'styled-system';
import { ColorProps, FlexboxProps, SpaceProps, LayoutProps, PositionProps, BackgroundProps, BordersProps, ShadowProps } from 'styled-system';
import {elevation, ElevationProps} from './style'


export type ImageBackgroundProps = ColorProps & FlexboxProps & SpaceProps & LayoutProps & PositionProps & BackgroundProps & BordersProps & ShadowProps & ElevationProps;
const ImageBackground = styled.ImageBackground<ImageBackgroundProps>`
  ${space};
  ${color};
  ${flexbox};
  ${layout};
  ${position};
  ${background};
  ${border};
  ${shadow};
  ${elevation};
`;

export default ImageBackground;
