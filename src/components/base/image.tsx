import styled from 'styled-components/native';
import { flexbox, space, layout, position, border, shadow } from 'styled-system';
import { FlexboxProps, SpaceProps, LayoutProps, PositionProps, BordersProps, ShadowProps } from 'styled-system';
import {elevation, ElevationProps} from './style'


export type ImageProps = FlexboxProps & SpaceProps & LayoutProps & PositionProps & BordersProps & ShadowProps & ElevationProps;
const Image = styled.Image<ImageProps>`
  ${space};
  ${flexbox};
  ${layout};
  ${position};
  ${border};
  ${shadow};
  ${elevation};
`;
Image.defaultProps = {
  resizeMode: 'contain',
}

export default Image;
