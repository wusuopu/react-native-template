import React from 'react';
import { Keyboard } from 'react-native';
import AntdButton, { ButtonProps } from '@ant-design/react-native/lib/button';
import styled from 'styled-components/native';
import { color, flexbox, space, position, background, border, shadow } from 'styled-system';
import { ColorProps, FlexboxProps, SpaceProps, PositionProps, BackgroundProps, BordersProps, ShadowProps } from 'styled-system';


export type Props = ButtonProps & ColorProps & FlexboxProps & SpaceProps & PositionProps & BackgroundProps & BordersProps & ShadowProps;
const MyButton = styled(AntdButton)<Props>`
  ${space};
  ${color};
  ${flexbox};
  ${position};
  ${background};
  ${border};
  ${shadow};
`;

export default class Button extends React.PureComponent<Props> {
  onPress = (e) => {
    // 当前为 loading 状态下不可以点击
    if (this.props.loading) { return }
    this.props.onPress && this.props.onPress(e)
  }
  render() {
    const { onPress, ...props } = this.props
    return (
      <MyButton {...props} onPress={this.onPress} />
    )
  }
};

// 表单按钮，点击之后隐藏软键盘
export class FormButton extends Button {
  onPress = (e) => {
    // 当前为 loading 状态下不可以点击
    if (this.props.loading) { return }
    Keyboard.dismiss()
    this.props.onPress && this.props.onPress(e)
  }
}

