import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { actions } from '@storybook/addon-actions';
import { ScrollView } from 'react-native';
import SafeView from '../base/safe-view'
import View from '../base/view'
import Text from '../base/text'
import Tabs from './';

storiesOf('Tabs', module)
.add('default', () => (
  <SafeView>
    <View flex={1}>
      <Tabs
        tabs={[
          'tab1',
          'tab2',
          'tab3',
          'tab4',
          'tab5',
        ]}
        tabBarPosition="top"
      >
         <View>
           <Text>tab1</Text>
         </View>

         <View>
           <Text>tab2</Text>
         </View>

         <View>
           <Text>tab3</Text>
         </View>

         <View>
           <Text>tab4</Text>
         </View>

         <View>
           <Text>tab5</Text>
         </View>
      </Tabs>
    </View>

    <View flex={1}>
      <Tabs
        tabs={[
          'tab1',
          'tab2',
          'tab3',
          'tab4',
          'tab5',
          'tab6',
          'tab7',
        ]}
        tabBarSwipeable={true}
        tabBarPosition="bottom"
      >
         <View>
           <Text>tab1</Text>
         </View>

         <View>
           <Text>tab2</Text>
         </View>

         <View>
           <Text>tab3</Text>
         </View>

         <View>
           <Text>tab4</Text>
         </View>

         <View>
           <Text>tab5</Text>
         </View>

         <View>
           <Text>tab6</Text>
         </View>

         <View>
           <Text>tab7</Text>
         </View>
      </Tabs>
    </View>
  </SafeView>
))
