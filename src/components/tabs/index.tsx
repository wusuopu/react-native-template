import React from 'react';
import { ScrollView } from 'react-native';
import _ from 'lodash';
import View, { Col, Row } from '../base/view';
import Text from '../base/text';
import TouchItem from '../base/touch-item';
import size from '../../lib/size';


type TabsBarItemTitlType = React.ReactNode
type TabsBarItemProps = {
  title: TabsBarItemTitlType;
  active: boolean;
  onPress: () => void;
  extraProps: any;
}

const TabsBarItem = (props: TabsBarItemProps) => {
  let node
  if (_.isString(props.title)) {
    node = (
      <Text fontSize={5} textAlign="center" color={props.active ? 'primary' : '#333333'}>{props.title}</Text>
    )
  } else {
    node = props.title
  }

  return (
    <Col flex={1}>
      <TouchItem {...props.extraProps} onPress={props.onPress} flex={1} justifyContent="center" alignItems="center">
      {node}
      </TouchItem>
      <Row width="100%" height={3} backgroundColor={props.active ? 'primary' : '#cccccc'} />
    </Col>
  )
}

type TabsBarProps = {
  tabs: TabsBarItemTitlType[];
  swipeable: boolean;
  page: number;
  onTabClick: (tab: TabsBarItemTitlType, index: number) => void;
}
class TabsBar extends React.PureComponent<TabsBarProps> {
  static defaultProps = {
    tabs: [],
    swipeable: false,
  }

  handTabClick = (index: number) => () => {
    this.props.onTabClick(this.props.tabs[index], index)
  }
  render() {
    let extraProps = this.props.swipeable ? {
      px: size.convertSize(20),
    } : {}
    let content = (
      <Row bg="#cccccc" height={size.convertSize(42)}>
        {
          _.map(this.props.tabs, (tab, index) => (
            <TabsBarItem
              key={index}
              active={index === this.props.page}
              title={tab}
              onPress={this.handTabClick(index)}
              extraProps={extraProps}
            />
          ))
        }
      </Row>
    )

    if (this.props.swipeable) {
      content = (
        <Row bg="#cccccc" height={size.convertSize(42)}>
          <ScrollView
            style={{height: size.convertSize(42)}}
            contentContainerStyle={{backgroundColor: '#cccccc'}}
            horizontal={true}
          >{content}</ScrollView>
        </Row>
      )
    }

    return content
  }
}

type Props = {
  tabs: TabsBarItemTitlType[];
  tabBarSwipeable: boolean;
  tabBarPosition: 'top'|'bottom';
  initialPage: number;
  onChange?: (index: number) => void;

}
export default class Tabs extends React.PureComponent<Props> {
  static defaultProps = {
    tabBarSwipeable: false,
    initialPage: 0,
    tabBarPosition: 'bottom',
  }

  state = {
    page: this.props.initialPage,
  }

  handTabClick = (tab, index: number) => {
    this.setState({page: index})
    this.props.onChange && this.props.onChange(index)
  }

  render () {
    return (
      <View flex={1} flexDirection={this.props.tabBarPosition === 'top' ? 'column' : 'column-reverse'}>
        <TabsBar
          tabs={this.props.tabs}
          swipeable={this.props.tabBarSwipeable}
          page={this.state.page}
          onTabClick={this.handTabClick}
        />
        <View flex={1}>
          {this.props.children[this.state.page]}
        </View>
      </View>
    )
  }
}
