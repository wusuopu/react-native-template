import React from 'react';
import View, { ViewProps } from './base/view';
import size from '../lib/size';

export default class Divider extends React.PureComponent<ViewProps & {direction?: 'horizontal'|'vertical'}> {
  static defaultProps = {
    borderWidth: size.convertSize(1),
    borderStyle: 'solid',
    borderColor: '#F2F2F2',
    direction: 'horizontal',
  }
  render() {
    const {direction, ...props} = this.props
    if (direction === 'horizontal') {
      props.height = 0
    } else {
      props.width = 0
    }
    return (
      <View {...props} />
    )
  }
}
