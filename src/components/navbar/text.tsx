import React, { ReactElement } from 'react';
import _ from 'lodash';
import Text from '../base/text';
import size from '../../lib/size';

type Props = {
  title?: string | ReactElement,
  titleColor?: string,
  side: 'left' | 'right',
}

const NavbarText = (props: Props) => {
  if (!props.title) { return null }

  let style = {
    ml: 0,
    mr: 0,
  };
  if (props.side === 'left') {
    style.ml = size.convertSize(6);
  } else {
    style.mr = size.convertSize(6);
  }
  return (
    <Text
      fontWeight={500}
      fontSize={size.convertSize(16)}
      color={props.titleColor}
      {...style}
    >{props.title}</Text>
  );
}

export default NavbarText;
