import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { ScrollView } from 'react-native';
import SafeView from '../base/safe-view'
import View from '../base/view'
import Text from '../base/text'
import Navbar from '.';

storiesOf('Navbar', module).add('default', () => (
  <SafeView>
    <ScrollView>
      <View mb={10}>
        <Text>默认</Text>
        <Navbar title="标题" left="返回" right="注册" />
      </View>

      <View mb={10} bg="primary">
        <Text>深色背景</Text>
        <Navbar title="标题" titleColor="white" left="返回" leftColor="white" right="注册" rightColor="white" />
      </View>

      <View mb={10} bg="primary">
        <Text>深色背景</Text>
        <Navbar title="标题" titleColor="white" backTitle="返回" backTitleColor="white" right="注册" rightColor="white" />
      </View>

      <View mb={10}>
        <Text>隐藏返回按钮</Text>
        <Navbar title="标题" backButtonShown={false} />
      </View>
    </ScrollView>
  </SafeView>
));
