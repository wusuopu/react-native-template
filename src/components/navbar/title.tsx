import React, { ReactElement } from 'react';
import _ from 'lodash';
import View from '../base/view';
import Text from '../base/text';
import size from '../../lib/size';

type Props = {
  title?: string | ReactElement,
  titleColor?: string,
}

const NavbarTitle = (props: Props) => {
  if (!props.title) { return  null }
  let title;
  if (_.isString(props.title)) {
    title = (
      <Text
        fontWeight={600}
        fontSize={size.convertSize(18)}
        color={props.titleColor}
      >{props.title}</Text>
    )
  } else {
    title = props.title;
  }
  return (
    <View
      position="absolute"
      left={size.convertSize(56)}
      right={size.convertSize(56)}
      top={0}
      bottom={0}
      justifyContent="center"
      alignItems="center"
    >{title}</View>
  )
}

export default NavbarTitle;
