import React, { ReactElement } from 'react';
import styled from 'styled-components/native';
import _ from 'lodash';
import View, { ViewProps } from '../base/view';
import TouchItem from '../base/touch-item';
import Styles from '../../constants/styles';
import NavbarTitle from './title';
import NavbarText from './text';
import NavbarBackButton from './back-button';
import size from '../../lib/size';

export type NavProps = {
  border?: boolean,
  bg?: string,
  title?: string | ReactElement,
  titleColor?: string,
  backButtonShown?: boolean,
  backImage?: ReactElement,
  backTitle?: string | ReactElement,
  backTitleColor?: string,
  left?: string | ReactElement,
  leftColor?: string,
  onLeftPress?: (__: any) => void,
  right?: string | ReactElement,
  rightColor?: string,
  onRightPress?: (__: any) => void,
};

export const Header = styled(View).attrs({
  borderBottomColor: '#a7a7aa',
  width: '100%',
  height: Styles.navBarHeaderStyle.height,
  position: 'relative',
})<ViewProps>``;

export default class Navbar extends React.Component<NavProps> {
  static defaultProps: NavProps = {
    border: true,
    bg: 'transparent',
    titleColor: 'rgba(0, 0, 0, .9)',
    leftColor: '#037aff',
    backTitleColor: '#037aff',
    rightColor: '#037aff',
    backButtonShown: true,
  }
  renderLeft() {
    if (!_.isUndefined(this.props.left)) {
      if (_.isString(this.props.left)) {
        let ele = (
          <NavbarText title={this.props.left} titleColor={this.props.leftColor} side="left" />
        )
        if (this.props.onLeftPress) {
          ele = (
            <TouchItem onPress={this.props.onLeftPress} flex={1}>
              {ele}
            </TouchItem>
          )
        }
        return ele
      }

      return this.props.left;
    }

    if (!this.props.backButtonShown) { return null; }
    return (
      <TouchItem flex={1} onPress={this.props.onLeftPress}>
        <View
          width="100%"
          flex={1}
          flexDirection="row"
          justifyContent="flex-start"
          alignItems="center"
        >
          <NavbarBackButton image={this.props.backImage} color={this.props.backTitleColor} />
          <NavbarText title={this.props.backTitle} titleColor={this.props.backTitleColor} side="left" />
        </View>
      </TouchItem>
    )
  }
  renderRight() {
    if (!this.props.right) {
      return null
    }
    if (!_.isString(this.props.right)) {
      return this.props.right;
    }
    let ele = (
      <NavbarText title={this.props.right} titleColor={this.props.rightColor} side="right" />
    );
    if (!this.props.onRightPress) {
      return ele;
    }

    return (
      <TouchItem
        flex={1}
        height="100%"
        justifyContent="center"
        alignItems="flex-end"
        onPress={this.props.onRightPress}
      >{ele}</TouchItem>
    )
  }
  render() {
    return (
      <Header
        borderBottomWidth={this.props.border ? 0.5 : 0}
        bg={this.props.bg}
      >
        <NavbarTitle {...this.props} />
        <View
          position="absolute"
          left={0}
          top={0}
          bottom={0}
          minWidth={size.convertSize(76)}
          flexDirection="row"
          alignItems="center"
          justifyContent="flex-start"
        >
          {this.renderLeft()}
        </View>
        <View
          position="absolute"
          right={0}
          top={0}
          bottom={0}
          minWidth={size.convertSize(56)}
          flexDirection="row"
          alignItems="center"
          justifyContent="flex-end"
        >
          {this.renderRight()}
        </View>
      </Header>
    )
  }
};
