import React, { ReactElement } from 'react';
import _ from 'lodash';
import { Icon } from '@ant-design/react-native';
import size from '../../lib/size';

type Props = {
  image?: ReactElement,
  color?: string,
}


const NavbarBackButton = (props: Props) => {
  if (props.image) { return props.image; }
  return (
    <Icon
      name="left"
      size={size.convertSize(22)}
      color={props.color}
      style={{
        marginLeft: size.convertSize(3),
        marginRight: size.convertSize(-6),
      }}
    />
  )
}

export default NavbarBackButton;
