import styled from 'styled-components/native';
import { color, flexbox, space, layout, position, background, border, shadow } from 'styled-system';
import { ColorProps, FlexboxProps, SpaceProps, LayoutProps, PositionProps, BackgroundProps, BordersProps, ShadowProps } from 'styled-system';
import { Keyboard } from 'react-native';

export type ViewProps = ColorProps & FlexboxProps & SpaceProps & LayoutProps & PositionProps & BackgroundProps & BordersProps & ShadowProps;
// 点击隐藏软键盘，仅能有一个子结点
const DismissKeyboardView = styled.TouchableWithoutFeedback.attrs(props => ({
  onPress: Keyboard.dismiss,
  accessible: false,
}))<ViewProps>`
  ${space};
  ${color};
  ${flexbox};
  ${layout};
  ${position};
  ${background};
  ${border};
  ${shadow};
`;
DismissKeyboardView.displayName = 'DismissKeyboardView';

export default DismissKeyboardView;
