import React from 'react';
import { Platform } from 'react-native';
import _ from 'lodash';
import { RefreshControl, FlatList, ActivityIndicator } from 'react-native';
import styled from 'styled-components/native';
import View from '../base/view';
import Text from '../base/text';

const DescriptionText = styled(Text).attrs({
  fontSize: 2,
})``;


type Props = {
  data: Array<any>;
  itemComponent?: any;
  renderItem?: (data: {item: any}) => any;
  keyExtractor?: (item: any, index: number) => string;
  inverted?: boolean;
  onRefresh?: () => Promise<any>;
  onLoadMore?: () => Promise<any>;
  contentContainerStyle?: any;
}

export default class PullView extends React.PureComponent<Props> {
  state = {
    refreshing: false,
    footerRefreshing: false,
    canLoadMore: true,
    loadMoreText: '上拉加载更多',
  }
  willLoadMore = false;

  renderItem = ({item}) => {
    if (this.props.itemComponent) {
      let Item = this.props.itemComponent
      return (<Item {...item} />)
    }
    if (this.props.renderItem) {
      return this.props.renderItem(item)
    }
  }
  keyExtractor = (item, index: number) => {
    if (this.props.keyExtractor) {
      return this.props.keyExtractor(item, index)
    }
    return index.toString()
  }
  onRefresh = async () => {
    if (this.state.refreshing) { return }
    if (this.props.onRefresh) {
      this.setState({refreshing: true})
      await this.props.onRefresh()
      this.setState({refreshing: false})
    }
  }
  onLoadMore = async () => {
    if (!this.state.canLoadMore) { return }
    if (this.state.footerRefreshing) { return }
    if (this.props.onLoadMore) {
      this.setState({footerRefreshing: true})
      let newState = {}
      if (await this.props.onLoadMore() === false) {
        newState = {canLoadMore: false, loadMoreText: '没有更多数据'}
      }
      this.setState(_.assign(newState, {footerRefreshing: false}))
    }
  }
  onScroll = (ev) => {
    let offsetY = ev.nativeEvent.contentOffset.y + ev.nativeEvent.layoutMeasurement.height
    this.willLoadMore = (offsetY > (ev.nativeEvent.contentSize.height + 100))
  }
  onScrollEndDrag = () => {
    if (this.willLoadMore) {
      this.onLoadMore()
    }
  }
  onEndReached = () => {
    // android 系统 onScroll 事件无效，则通过 onEndReached 实现上拉加载
    this.onLoadMore()
  }

  renderListFooter = () => {
    if (!this.props.onLoadMore) {
      return null
    }

    let item = (
      <DescriptionText>{this.state.loadMoreText}</DescriptionText>
    )
    if (this.state.footerRefreshing) {
      item = (
        <ActivityIndicator
          size="large"
          animating={true}
        />
      )
    }

    return (
      <View
        flex={1}
        flexDirection="row"
        justifyContent="center"
        alignItems="flex-end"
        py={10}
      >
        {item}
      </View>
    )
  }
  renderListEmpty = () => {
    return (
      <View
        width="100%"
        flexDirection="row"
        justifyContent="center"
        alignItems="center"
        pt={10}
      >
        <DescriptionText>暂无数据</DescriptionText>
      </View>
    )

  }
  render() {
    let refreshControl
    if (this.props.onRefresh) {
      refreshControl = (
        <RefreshControl
          onRefresh={this.onRefresh}
          refreshing={this.state.refreshing}
          title="下拉加载"
        />
      )
    }
    let contentContainerStyle = {}
    let isAndroid = Platform.OS === 'android'
    if (isAndroid) {
      contentContainerStyle = {minHeight: '102%'}
    }
    return (
      <FlatList
        contentContainerStyle={[contentContainerStyle, this.props.contentContainerStyle]}
        data={this.props.data}
        renderItem={this.renderItem}
        keyExtractor={this.keyExtractor}
        inverted={this.props.inverted}
        refreshControl={refreshControl}
        onScroll={isAndroid ? undefined : this.onScroll}
        onScrollEndDrag={this.onScrollEndDrag}
        onEndReached={isAndroid ? this.onEndReached : undefined}
        onEndReachedThreshold={0.01}
        ListFooterComponent={this.renderListFooter}
        ListFooterComponentStyle={{flex: 1}}
        ListEmptyComponent={this.renderListEmpty}
        scrollEnabled={!(this.state.refreshing || this.state.footerRefreshing)}
      />
    )
  }
}
