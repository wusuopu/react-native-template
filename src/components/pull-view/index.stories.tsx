import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { action } from '../../../storybook/addons/actions';
import SafeView from '../base/safe-view'
import View from '../base/view'
import Text from '../base/text'
import PullView from '.';

storiesOf('PullView', module)
  .add('default', () => (
    <SafeView height="100%">
      <PullView
        data={[
          {children: 'First Item'},
          {children: 'Second Item' },
          {children: 'Third Item' },
        ]}
        itemComponent={Text}
        onRefresh={action('onRefresh')}
        onLoadMore={action('onLoadMore')}
      />
    </SafeView>
  ))
  .add('空列表', () => (
    <SafeView height="100%">
      <PullView
        data={[]}
        itemComponent={Text}
        onRefresh={action('onRefresh')}
        onLoadMore={action('onLoadMore')}
      />
    </SafeView>
  ));

