import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { actions } from '@storybook/addon-actions';
import { ScrollView } from 'react-native';
import SafeView from '../base/safe-view'
import View from '../base/view'
import Text from '../base/text'
import TabBar from '.';

storiesOf('TabBar', module).add('default', () => (
  <SafeView>
    <ScrollView>
      <View mb={10}>
        <Text>默认</Text>
        <TabBar
          items={[
            {title: '标签1', icon: 'edit', selected: true},
            {title: '标签2', icon: 'form', },
            {title: '标签3', icon: 'copy', },
            {title: '标签4', icon: 'copy', },
          ]}
          {...actions({onPress: 'select'})}
        />
      </View>

      <View mb={10}>
        <Text>深色背景</Text>
        <TabBar
          bg="blue"
          unselectedTintColor="white"
          tintColor="red"
          items={[
            {title: '标签1', icon: 'edit', selected: true},
            {title: '标签2', icon: 'form', },
            {title: '标签3', icon: 'copy', },
            {title: '标签4', icon: 'copy', },
          ]}
          {...actions({onPress: 'select'})}
        />
      </View>
    </ScrollView>
  </SafeView>
));

