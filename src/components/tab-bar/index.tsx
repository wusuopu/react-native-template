import React from 'react';
import { ImageProps } from 'react-native'
import _ from 'lodash';
import styled from 'styled-components/native';
import View from '../base/view';
import Text from '../base/text';
import Image from '../base/image';
import TouchItem from '../base/touch-item';
import { Icon } from '@ant-design/react-native';
import size from '../../lib/size';

type TabBarItem = {
  title: string;
  icon?: React.ReactNode | string | number;
  selectedIcon?: React.ReactNode | string | number;
  badge?: string | number;
  selected?: boolean;
}
type TabBarItemProps = {
  item: TabBarItem;
  tintColor: string;              // selected's font color
  unselectedTintColor: string;    // unselected's font color
  onPress?: () => void;
}
export type TabBarProps = {
  bg: string;
  tintColor: string;              // selected's font color
  unselectedTintColor: string;    // unselected's font color
  items: TabBarItem[];
  onPress?: (key: any) => void;
}

const TabItemIcon = (props: TabBarItemProps) => {
  let icon = props.item.selected ? props.item.selectedIcon || props.item.icon : props.item.icon;
  let color = props.item.selected ? props.tintColor : props.unselectedTintColor;
  if (!icon) {
    // 没有指定图标
    return null;
  }
  let width = size.convertSize(28)
  let height = size.convertSize(28)
  if (_.isString(icon)) {
    return (
      <Icon
        name={icon}
        style={{fontSize: size.convertSize(20), color}}
      />
    );
  }
  if (_.isNumber(icon)) {
    return (
      <Image
        width={width}
        height={height}
        source={icon}
      />
    )
  }
  return icon;
}
const TabItem = (props: TabBarItemProps) => {
  return (
    <TouchItem
      flex={1}
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
      height="100%"
      onPress={props.onPress}
    >
      <TabItemIcon {...props} />
      <Text
        fontSize={size.convertSize(12)}
        color={props.item.selected ? props.tintColor : props.unselectedTintColor}
      >{props.item.title}</Text>
    </TouchItem>
  )
}

export default class TabBar extends React.PureComponent<TabBarProps> {
  static defaultProps = {
    bg: '#ebeeef',
    tintColor: '#108ee9',
    unselectedTintColor: '#888888',
    items: [],
  }

  renderItems() {
    let items = _.map(this.props.items, (item, index) => {
      return (
        <TabItem
          key={index}
          item={item}
          tintColor={this.props.tintColor}
          unselectedTintColor={this.props.unselectedTintColor}
          onPress={this.handlePress(index)}
        />
      )
    });
    return items;
  }
  render() {
    return (
      <View
        width="100%"
        height={size.convertSize(50)}
        flexDirection="row"
        flexWrap="nowrap"
        bg={this.props.bg}
      >
        {this.renderItems()}
      </View>
    )
  }

  handlePress = (index: any) => {
    return () => {
      this.props.onPress && this.props.onPress(index)
    }
  }
}
