import { init, RematchRootState } from '@rematch/core';
import createLoadingPlugin from '@rematch/loading';
import selectPlugin from '@rematch/select';
import createRematchPersist from '@rematch/persist'
import AsyncStorage from '@react-native-community/async-storage';
import app from './app'

const persistPlugin = createRematchPersist({
  whitelist: [],
  throttle: 500,
  version: 1,
  storage: AsyncStorage,
  key: 'rematch:root',
})

const models = {
  app,
}

const store = init({
  models,
  plugins: [
    selectPlugin(),
    createLoadingPlugin({
      asNumber: true,
      blacklist: [
        'app/route$navigate',
        'app/route$reset',
        'app/route$push',
        'app/route$back',
        'app/ui$toast'
      ],
    }),
    persistPlugin
  ]
})

export default store

// 将 store 设为全局变量
globalThis.Store = store

export type Store = typeof store
export type Select = typeof store.select
export type Dispatch = typeof store.dispatch
export type iRootState = RematchRootState<typeof models>
