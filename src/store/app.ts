import _ from 'lodash'
import { StatusBarProps } from 'react-native';
import { createModel } from '@rematch/core'
import { Toast } from '@ant-design/react-native';
import NavigationService from '../navigation-service'
import Theme from '../theme'
import timer from '../lib/timer'

type NavigationPayload = {
  routeName: string,
  params?: object,
}

type State = {
  statusBar: StatusBarProps;
}
const initState: State = {
  statusBar: {}
}

export default createModel({
  state: initState,
  reducers: {
    ui$statusBar(prevState: State, payload: StatusBarProps): State {
      if (payload.backgroundColor && Theme.colors[payload.backgroundColor]) {
        // 使用 theme 定义的颜色
        payload.backgroundColor = Theme.colors[payload.backgroundColor]
      }
      return _.assign({}, prevState, {statusBar: payload})
    },
  },
  effects: {
    // 路由切换操作
    async route$navigate(payload: {routeName: string | object, params?: object, action?: object}): Promise<boolean> {
      await timer.sleep(5)
      return NavigationService.navigate(payload.routeName, payload.params, payload.action)
    },
    async route$reset(payload: NavigationPayload): Promise<boolean> {
      await timer.sleep(5)
      return NavigationService.reset(payload.routeName, payload.params)
    },
    async route$push(payload: NavigationPayload): Promise<boolean> {
      await timer.sleep(5)
      return NavigationService.push(payload.routeName, payload.params)
    },
    async route$back(key?: string): Promise<boolean> {
      await timer.sleep(5)
      return NavigationService.back(key)
    },
    // 显示 toast 提示
    async ui$toast(payload: {content: any, duration?: number, onClose?: () => void, mask?: boolean, action?: 'success' | 'fail' | 'info' | 'loading' | 'offline'}): Promise<number> {
      let { action, content, duration, onClose, mask } = payload
      if (_.isUndefined(duration)) { duration = 3 }
      if (_.isUndefined(mask)) { mask = true }
      if (_.isUndefined(action)) { action = 'info' }
      return await Toast[action](content, duration, onClose, mask)
    },
  }
})
