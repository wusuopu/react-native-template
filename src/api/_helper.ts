import { AxiosRequestConfig } from 'axios';
import _ from 'lodash';
import qs from 'query-string';
import http from './_http';
import Config from '../constants/config';
import timer from '../lib/timer';
import config from '../constants/config';

// 启用 mock api
const MOCK_API = Config.MOCK_API;

const serializeBody = (config: AxiosRequestConfig): AxiosRequestConfig => {
  if (config.data && !_.get(config.headers, 'Content-Type')) {
    // 默认使用 x-www-form-urlencoded 格式提交数据
    config.headers = config.headers || {}
    config.headers['Content-Type'] = 'application/x-www-form-urlencoded'
    config.data = qs.stringify(config.data)
  }

  return config
}

const authRequest = async (config: AxiosRequestConfig) => {
  config = serializeBody(config)
  try {
    return await http.authHTTP.request(config)
  } catch (error) {
    if (error.code !== 403) {
      throw error;
    }
  }

  // 当前 token 失效，重新获取 token
  try {
    await globalThis.Store.dispatch.auth.refreshTokenAsync()
  } catch (error) {
    if (error.code !== 403) {
      throw error;
    }
    // uuid 失效，返回登录页面重新登录
    globalThis.Store.dispatch.navigation.reset({routeName: 'Login'})
    return
  }

  // 使用新的 token 重新调用 api
  return await http.authHTTP.request(config)
}

const guestRequest = (config: AxiosRequestConfig) => {
  config = serializeBody(config)
  return http.guestHTTP.request(config)
}

const createMockResponse = async (data: any, status: number = 200, message: string = '') => {
  await timer.sleep(2000);
  return {
    status,
    data: {
      code: status,
      message,
      data
    }
  }
}


export default {
  MOCK_API,
  createMockResponse,
  authRequest,
  guestRequest,
}
