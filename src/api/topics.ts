import axios from 'axios'
import _ from 'lodash';
import helper from './_helper';
import APIMock from './topics.mock';

let API = {
  index() {
    return axios.get('https://cnodejs.org/api/v1/topics')
  }
}

if (helper.MOCK_API) {
  _.assign(API, APIMock)
}
export default API
