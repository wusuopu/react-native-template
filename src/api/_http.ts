import axios, { AxiosInstance, AxiosResponse } from 'axios';
import Config from '../constants/config';

export interface HTTPError extends Error {
  response?: AxiosResponse,
  code?: number,
  isHTTPError?: boolean,
}

const createError = (response: AxiosResponse): HTTPError => {
  let error:HTTPError = new Error(response.data.message || '请求失败，请稍后再试')
  error.code = response.data.code || response.status
  error.response = response

  return error
}

// 匿名用户的请求
const guestHTTP: AxiosInstance = axios.create({
  baseURL: Config.API_HOST,
  timeout: 5000,
  // validateStatus: function (status) {
  //   return status >= 200 && status < 600;
  // },
})
// 登录用户的请求
const authHTTP: AxiosInstance = axios.create({
  baseURL: Config.API_HOST,
  timeout: 5000,
  // validateStatus: function (status) {
  //   return status >= 200 && status < 600;
  // },
})

// api错误处理
function responseInterceptor (response: AxiosResponse) {
  if (response.data.code !== 200) {
    return Promise.reject(createError(response))
  }
  return response
}
function errorInterceptor (error) {
  error.code = error.response?.data?.code || error.response?.status
  error.message = error.response?.data?.message || '请求失败，请稍后再试'
  return Promise.reject(error);
}
authHTTP.interceptors.response.use(responseInterceptor, errorInterceptor)
guestHTTP.interceptors.response.use(responseInterceptor, errorInterceptor)

const setAuthToken = (token?: string) => {
  // 设置 api 请求的 token
  authHTTP.defaults.headers.common['Token'] = token
}


export default {
  guestHTTP,
  authHTTP,

  setAuthToken,
}
