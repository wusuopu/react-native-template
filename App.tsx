import React from 'react';
import Orientation from 'react-native-orientation-locker';
import { Provider as AntdProvider } from '@ant-design/react-native';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { getPersistor } from '@rematch/persist'
import { PersistGate } from 'redux-persist/lib/integration/react'
import Store from './src/store';
import Routes from './src/routes';
import theme, { AntdTheme } from './src/theme';
import NavigationService from './src/navigation-service';
import StatusBar from './src/containers/status-bar';

if (!__DEV__) {
  // 在 release 模式下禁用日志输出
  globalThis.console.log = () => {};
  globalThis.console.debug = () => {};
  globalThis.console.info = () => {};
  globalThis.console.warn = () => {};
}

const persistor = getPersistor()

export default class App extends React.Component {
  constructor (props: any) {
    super(props)

    // 锁定当前 App 只能使用竖屏
    Orientation.lockToPortrait()
  }
  render () {
    return (
      <Provider store={Store}>
        <ThemeProvider theme={theme}>
          <PersistGate persistor={persistor}>
            <AntdProvider theme={AntdTheme}>
              <StatusBar />
              <Routes ref={ref => NavigationService.setTopLevelNavigator(ref)} />
            </AntdProvider>
          </PersistGate>
        </ThemeProvider>
      </Provider>
    );
  }
}
