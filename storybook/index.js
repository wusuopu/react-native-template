import React from 'react';
import { getStorybookUI, configure, addDecorator } from '@storybook/react-native';
import { ThemeProvider } from 'styled-components';
import { Provider as AntdProvider } from '@ant-design/react-native';
import theme, { AntdTheme } from '../src/theme';

import './rn-addons';

// import stories

addDecorator((storyFn) => (
  <ThemeProvider theme={theme}>
    <AntdProvider theme={AntdTheme}>
      { storyFn() }
    </AntdProvider>
  </ThemeProvider>
));
configure(() => {
  // 在此添加新的 stories
  require('../src/stories');
}, module);

// Refer to https://github.com/storybookjs/storybook/tree/master/app/react-native#start-command-parameters
// To find allowed options for getStorybookUI
const StorybookUIRoot = getStorybookUI({});

export default StorybookUIRoot;
